#!/usr/bin/python3

# queue-viewer -- Generates HTML queue overviews for the
#   Debian proposed-updates queues as seen by the release team.
#
# (C) 2007-2011 Philipp Kern <pkern@debian.org>
# (C) 2009-2019 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# Dependencies (expressed in Debian packages):
#  * python3-debian
#  * python3
#  * python3-jinja2 (for ouput generation)
#  * python3-psycopg2 (for projectb access)
#  * devscripts (for the debdiffing facility)
#  * lintian
#  * dose-distcheck (for dependency checking)

"""
This program generates queue overviews for the Debian proposed-updates
queues as seen by the release team.

The configuration of this script is done in `config.ini'.  An example is
provided below:

[suite-name]
directory = /path/to/the/queue
comments_directory = /path/to/the/queue/COMMENTS
xml_output = /org/release.debian.org/webroot/suite-name.xml
xslt_template = queue-template.xslt
html_output = /org/release.debian.org/webroot/suite-name.html
release_architectures = source all i386 amd64

The appearance of the HTML output is defined by the provided Jinja template.
The XML file, which is used as the base for the template transformation,
should help developers with no access to the original data to use the queue
information more easily than by employing screen scraping on the generated
HTML.  It is guaranteed that it contains all information the HTML output
provides.

This script also needs access to Debian's projectb PostgreSQL database
which holds all information about packages currently installed into the
archive.
"""

# TODO:
#  * Provide per arch/suite lists of missing builds (maybe on another page
#    linked by the one listing all packages): for example
#    oldstable-security/alpha, oldstable-proposed-updates/alpha, ...
#  * Clean up the diff directory to remove now superfluous diffs.
#  * Show the changes file on binNMUs.

from __future__ import print_function

import os
import sys
import time
import logging

from collections import defaultdict
from shutil import copyfile
from tempfile import mkstemp
from six.moves.configparser import ConfigParser

from debian.debian_support import Version

# add the base path to fetch our central libraries
sys.path.append(os.path.dirname(__file__) + '/../lib/')

from debrelease import normalize_path
from debrelease.projectb import ProjectB
from debrelease.proposed import QueueParser
from debrelease.piuparts import PiupartsFactory
from debrelease.jinja import jinja
from debrelease.ci import CIFactory
from debrelease.helpers import Helpers


def jinja_convert(template_filename, output_filename, **kwargs):
    with open(normalize_path(template_filename), encoding='utf-8') as template_file:
        template = list(template_file)
    with open(output_filename, 'w', encoding='utf-8') as output:
        output.write(jinja().render_jinja_template(template, **kwargs))


def read_config():
    # Use first parameter as config file if exists,
    # else config.ini in the same directory as the program.
    if sys.argv[1:] and os.path.isfile(sys.argv[1]):
        config_file = sys.argv[1]
    else:
        config_file = os.path.dirname(__file__) + '/config.ini'

    config = ConfigParser()
    if not config.read([config_file]):
        print('E: could not read %s' % (config_file,), file=sys.stderr)
        sys.exit(1)
    return config


class QueueViewer():
    def __init__(self):
        self.config = read_config()
        self.projectB = ProjectB()
        loglevel = logging.ERROR
        logger = logging.getLogger('queue-viewer')
        self.logger = logger
        if 'logging' in self.config.sections():
            loglevel_s = self.config.get('logging', 'level').upper()
            loglevel = getattr(logging, loglevel_s, None)
            if not isinstance(loglevel, int):
                raise ValueError('Invalid log level: %s' % loglevel_s)
            logfile_s = self.config.get('logging', 'filename')
            if logfile_s:
                logfile = time.strftime(logfile_s, time.gmtime())
                file_handler = logging.FileHandler(logfile)
                formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s: %(message)s')
                file_handler.setFormatter(formatter)
                logging.getLogger().addHandler(file_handler)
        logger.setLevel(loglevel)
        debrelease_logger = logging.getLogger("debrelease")
        debrelease_logger.setLevel(loglevel)
        debrelease_logger.propagate = True

    def main(self):
        self.logger.info('Startup')
        # Multiple sections with different suites could be placed in the configuration
        # file.  Iterate over all of them.
        # Some sections are not suites, so we need to ignore them.
        ignore_sections = ['projectb', 'dak', 'security', 'mail', 'piuparts', 'logging', 'ci']
        for suite in self.config.sections():
            if suite in ignore_sections or \
                  not self.config.getboolean(suite, 'supported', fallback='yes'):
                self.logger.debug("Skipping section '%s'", suite)
                continue
            self.process_suite(suite)
        self.logger.info('Shutdown')

    def process_suite(self, suite):
        self.logger.info("Processing suite '%s'", suite)
        # Initialise the queue parser: this already processes the queue directory
        # in question.
        parser = QueueParser(self.projectB, self.config, suite)

        # Initialise the needed factories
        piuparts = PiupartsFactory(self.config, suite)
        ci = CIFactory(self.config, suite)
        diff_helpers = Helpers(self.projectB, self.config, suite)

        # If the package has been accepted, the fact that the
        # updates suite and above suite have the same version
        # most likely means that the package from the updates
        # suites was propagated upwards.
        # In any case, the updates suite version must not be
        # higher than the above suite.
        self.logger.info("Checking for versioning issues")
        version_issues = {
                         '%s_%s/%s' % (item.package, item.left_version, item.architecture): item
                         for item in
                         self.projectB.compare_suites_op(suite, '>', parser.above_suite)
                         +
                         self.projectB.compare_suites_op(parser.policy_suite, '>=', parser.above_suite)
                         }

        updated = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime())

        # Add package list with comments
        self.logger.info("Packages")
        entry_details = {}
        actions = {'': [], 'ACCEPT': [], 'ACCEPTED': [], 'REJECT': [], 'REJECTED': []}
        for package in sorted(parser.entries):
            self.logger.debug("Processing package %s", package)
            entry_details[package] = {}

            for version in sorted(parser.entries[package]):
                self.logger.info("Processing %s_%s", package, version)
                info = parser.entries[package][version]
                entry_details[package][version] = defaultdict(list)
                entry = entry_details[package][version]

                actions[info.get('action', '')].append((package, version))

                fullversion = info.get('fullversion', version)
                entry['display_version'] = version if 'binNMU' in info else fullversion

                if 'architectures' in info:
                    # produce binary package diffs
                    if 'superseded' in info:
                        arch_list = info['missing_builds']
                    else:
                        arch_list = info['architectures'] - {'source'}
                    for arch in sorted(arch_list & {binarch for binarch in info.get('binaries', [])}):
                        pending, interesting, exc = \
                            diff_helpers.do_debcheck(package, version, arch, info)

                        if interesting:
                            entry['debchecks'].append(arch)
                        elif pending:
                            entry['debchecks'].append("%s-needed" % (arch))
                        if exc:
                            print('W: ignoring debcheck of %s_%s_%s: %s'
                                  % (package, version, arch, exc), file=sys.stderr)

                        pending, interesting, exc = \
                            diff_helpers.do_lintian_diff(package, version, arch, info)

                        if interesting:
                            entry['lintian'].append(arch)
                        elif pending:
                            entry['lintian'].append("%s-needed" % (arch))
                        if exc:
                            print('W: ignoring binary lintian of %s_%s_%s: %s'
                                  % (package, version, arch, exc), file=sys.stderr)

                        pending, interesting, exc = \
                            diff_helpers.do_binary_diff(package, version, arch, info)

                        if interesting:
                            entry['binary_debdiffs'].append(arch)
                        elif pending:
                            entry['binary_debdiffs'].append("%s-needed" % (arch))
                        if exc:
                            print('W: ignoring binary debdiff of %s_%s_%s: %s'
                                  % (package, version, arch, exc), file=sys.stderr)

                if info.get('action', '').startswith('REJECT'):
                    continue

                if 'base_fullversion' in info:
                    # If we have no source, then it's OK for the updates
                    # suite and base suite to have the same version.
                    # In any case, the updates suite version must not be
                    # lower than the base suite.
                    isok = 'source' not in info.get('architectures', []) and \
                        fullversion == info['base_fullversion']
                    isok = isok or \
                        (Version(info['base_fullversion']) < Version(fullversion))
                    if not isok:
                        entry['version_problems'].append(
                            '%s (%s)' % (parser.base_suite, info['base_fullversion']))
                source_key = '%s_%s/source' % (package, fullversion)
                if source_key in version_issues:
                    entry['version_problems'].append(
                         '%s (%s)' % (parser.above_suite, version_issues[source_key].right_version))
                if 'binaries' in info and 'superseded' not in info:
                    self.logger.debug("Version issues: binary")
                    for binary in sorted({binary for arch in info['binaries']
                                         for binary in info['binaries'][arch]}):
                        self.logger.debug("Package %s", binary)
                        for arch in sorted(info['binaries']):
                            self.logger.debug("%s_%s", binary, arch)
                            if binary in info['binaries'][arch] and \
                               'proposed' in info['binaries'][arch][binary]:
                                bininfo = info['binaries'][arch][binary]
                            else:
                                continue
                            if 'base' in bininfo:
                                if Version(bininfo['base'][0]) >= Version(bininfo['proposed'][0]):
                                    # If the source and binary versions in p-u{,-new}
                                    # match and so do the versions in the base suite,
                                    # neither is a binNMU and any version problems
                                    # will already have been handled by the source
                                    # version check
                                    if not ((info['base_fullversion'] == bininfo['base'][0]) and
                                            (fullversion == bininfo['proposed'][0])):
                                        if bininfo['proposed'][0] == fullversion:
                                            version_slug = ''
                                        else:
                                            version_slug = '%s/' % (bininfo['proposed'][0], )
                                        entry['version_problems'].append(
                                            '%s/%s [%s] (%s%s)' % (binary,
                                                                   parser.base_suite,
                                                                   arch,
                                                                   version_slug,
                                                                   bininfo['base'][0]))
                            binary_key = '%s_%s/%s' % (binary,
                                                       bininfo['proposed'][0],
                                                       arch,
                                                       )
                            if binary_key in version_issues:
                                    # If the source and binary versions in p-u{,-new}
                                    # match and so do the versions in the above suite,
                                    # neither is a binNMU and any version problems
                                    # will already have been handled by the source
                                    # version check
                                    #
                                    # If there is no source in p-u{,-new} then it
                                    # cannot cause a version problem
                                    if not ((fullversion == bininfo['proposed'][0]) and
                                            (source_key not in version_issues or
                                             version_issues[source_key].right_version ==
                                             version_issues[binary_key].right_version)):
                                        if bininfo['proposed'][0] == fullversion:
                                            version_slug = ''
                                        else:
                                            version_slug = '%s/' % (bininfo['proposed'][0], )
                                        entry['version_problems'].append(
                                            '%s/%s [%s] (%s%s)' % (binary,
                                                                   parser.above_suite,
                                                                   arch,
                                                                   version_slug,
                                                                   version_issues[binary_key].right_version,
                                                                   )
                                                                )

                if 'superseded' in info or 'binNMU' in info:
                    continue

                if info.get('action', '') == 'ACCEPTED':
                    self.logger.debug("Checking for piuparts issues")
                    if package in piuparts['install'] and \
                       piuparts['install'][package] == 'fail':
                        self.logger.debug("Installation issue found")
                        entry['piuparts'].append('install')
                    if 'new_source' not in info and package in piuparts['upgrade'] and \
                       piuparts['upgrade'][package] == 'fail':
                        self.logger.debug("Upgrade issue found")
                        entry['piuparts'].append('upgrade')
                    self.logger.debug("Checking for CI issues")
                    if package in ci.statuses:
                        verdict, data = ci.statuses[package]
                        if data:
                            self.logger.debug("CI issue found")
                            entry['ci'].append((verdict, data))

                if 'new_source' in info:
                    continue

                # Create the source package and lintian diffs
                pending, interesting, exc = \
                    diff_helpers.do_lintian_diff(package, version, "source", info)

                if interesting:
                    entry['lintian'].append("source")
                elif pending:
                    entry['lintian'].append("source-needed")
                if exc:
                    print('W: ignoring source lintian of %s_%s_%s: %s'
                          % (package, version, arch, exc), file=sys.stderr)

                pending, interesting, exc = \
                    diff_helpers.do_source_debdiff(package, version, info)

                if interesting:
                    entry['debdiff'] = ['source']
                elif pending:
                    entry['debdiff'] = ['source-needed']
                if exc:
                    print('W: ignoring debdiff of %s_%s: %s'
                          % (package, version, exc), file=sys.stderr)

        # This is a horrible hack that is currently required
        # in order to allow us to serialise the "parser" object,
        # so that we can dump it as YAML.
        #
        # There must be a better solution, probably in
        # debrelease.jinja's "yaml" filter, and ideally this
        # can be removed once the better solution is in place.
        #
        # The hack works because we know that:
        # - it removes all non-serialisable members
        # - the members are not required beyond this point
        del parser.projectb
        del parser.logger

        for output in ['overview', 'missing', 'yaml']:
            try:
                _, temp_path = mkstemp()
                jinja_convert(self.config.get(suite, "jinja_%s_template" % (output)),
                              temp_path,
                              entries=parser.entries,
                              todos=parser.todos,
                              entry_details=entry_details,
                              actions=actions,
                              removals=parser.removals,
                              overview=parser,
                              updated=updated,
                              )
                copyfile(temp_path, self.config.get(suite, "jinja_%s_output" % (output)))
            except Exception as e:
                print('W: ignoring jinja %s output of %s: %s'
                      % (output, suite, e), file=sys.stderr)
            finally:
                os.remove(temp_path)
        self.logger.info("Finished processing suite '%s'", suite)


if __name__ == '__main__':
    QueueViewer().main()

# vim:set sw=4 et:
# -*- Mode: python; coding: utf-8; tab-width: 4; indent-tabs-mode: f; -*-
