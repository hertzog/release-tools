#!/bin/bash

SUITE="$1"

SECURITYBASE="http://security.debian.org"

: > /srv/release.debian.org/tmp/${SUITE}-Packages-security
: > /srv/release.debian.org/tmp/${SUITE}-Packages-proposed
: > /srv/release.debian.org/tmp/${SUITE}-Sources-security
: > /srv/release.debian.org/tmp/${SUITE}-Sources-proposed

for arch in $(dak admin s-a list-arch ${SUITE}); do
	url=${SECURITYBASE}/dists/${SUITE}/updates/main/binary-${arch}/Packages.xz
        file=/srv/release.debian.org/tmp/${arch}-Packages.xz
        wget $url -q -O ${file}
        [ -s "$file" ] || continue
        xzcat $file >> /srv/release.debian.org/tmp/${SUITE}-Packages-security
	echo >> /srv/release.debian.org/tmp/${SUITE}-Packages-security

	for file in \
	  /srv/ftp-master.debian.org/mirror/dists/${SUITE}-proposed-updates/main/binary-${arch}/Packages.xz \
	  /srv/ftp-master.debian.org/policy/dists/${SUITE}-new/main/binary-${arch}/Packages.xz \
	  /srv/ftp-master.debian.org/mirror/dists/${SUITE}/main/binary-${arch}/Packages.xz; do
		[ -s "$file" ] || continue
		xzcat $file >> /srv/release.debian.org/tmp/${SUITE}-Packages-proposed
		echo >> /srv/release.debian.org/tmp/${SUITE}-Packages-proposed
	done
done

wget -q ${SECURITYBASE}/dists/${SUITE}/updates/main/source/Sources.xz -O - | xzcat > /srv/release.debian.org/tmp/${SUITE}-Sources-security

for file in /srv/ftp-master.debian.org/mirror/dists/${SUITE}-proposed-updates/main/source/Sources.xz \
            /srv/ftp-master.debian.org/policy/dists/${SUITE}-new/main/source/Sources.xz \
            /srv/ftp-master.debian.org/mirror/dists/${SUITE}/main/source/Sources.xz; do
            [ -s "$file" ] && xzcat $file >> /srv/release.debian.org/tmp/${SUITE}-Sources-proposed
done
