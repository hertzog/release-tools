#!/bin/bash
#
# Copyright © 2016 Emilio Pozuelo Monfort <pochu@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

set -e
set -u

if [ $# != 5 ]
then
    echo "Usage: `basename $0` (accept|reject) (pu|opu|oopu) package version \"reason\""
    echo
    echo "(For packages from security.d.o, \"reason\" should simply be the numeric DSA reference."
    exit 42
fi

COMMAND="${1^^}"
QUEUE="${2}"
PACKAGE="${3}"
VERSION="${4}"
REASON="${5}"

if [[ $PACKAGE =~ ^(ACCEPT|ACCEPTED|REJECT|REJECTED). ]]
then
    echo "Supplied \"package\" appears to be an already processed comment file."
    exit 42
fi

case "${QUEUE}" in
    pu|p-u)
        QUEUE="p-u-new"
        ;;
    opu|o-p-u)
        QUEUE="o-p-u-new"
        ;;
    oopu|o-o-p-u)
        QUEUE="o-o-p-u-new"
        ;;
    p-u-new|o-p-u-new|o-o-p-u-new)
        ;;
    *)
        echo "Invalid policy queue ${QUEUE}."
        exit 42
        ;;
esac

DSAPREFIX=""
if [[ $REASON =~ ^[0-9]+$ ]]
then
    DSAPREFIX="DSA ${REASON} "
    REASON="security update"
fi

case "${COMMAND}" in
    ACCEPT)
        REASON="$(printf "%s\n%s\n" "OK" "${DSAPREFIX}${PACKAGE} - ${REASON}")"
        ;;
    REJECT)
        REASON="$(printf "%s\n%s\n" "NOTOK" "${DSAPREFIX}${PACKAGE} - ${REASON}")"
        ;;

    *)
        echo "Invalid command ${COMMAND}."
        exit 42
        ;;
esac

echo "${REASON}" | ssh -2 -T -o BatchMode=yes -i ~release/sets/ssh/p-u-comment dak@fasolo.debian.org ${COMMAND} ${QUEUE} ${PACKAGE}_${VERSION}
