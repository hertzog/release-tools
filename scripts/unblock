#!/bin/bash
#
# Copyright © 2017 Emilio Pozuelo Monfort <pochu@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

set -e
set -u

package=$1
user=`id -un`
hintfile=/srv/release.debian.org/britney/hints/$user

d $package || true

# FIXME use d's output? It is smarter and checks for unblock-udebs

while true
do
    echo -n "Unblock $package? (Y/n) "
    read ret
    case "$ret" in
        n|N) exit 1 ;;
        y|Y|"") break ;;
    esac
done

unblock=`hint unblock $package`

while true
do
    echo -n "Age $package? (N/0-9) "
    read ret
    case "$ret" in
        n|N|"") break ;;
        [0123456789]) days=$ret; break ;;
    esac
done

if test -n "${days:-}"
then
    age=`hint age-days $days $package`
    unblock="${unblock}\n${age}"
fi

bugs=`bts select package:release.debian.org users:release.debian.org@packages.debian.org usertags:unblock status:open 2>/dev/null`

while read line
do
    # FIXME: what if the request doesn't have the standard format?
    bts status $line fields:subject | grep -q " ${package}/" && bug=$line && break
done <<< "$(echo -e "$bugs")"

if test -n "${bug:-}"
then
    unblock="# #${bug}\n${unblock}"
    echo "Unblocked." | mail -s "unblock $package" ${bug}-done@bugs.debian.org
fi

date=`date +%Y%m%d`
sed -i "1s;^;# ${date}\n${unblock}\n\n;" $hintfile
